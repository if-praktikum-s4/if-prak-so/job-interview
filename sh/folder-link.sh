#!/bin/bash

# Cek argumen 1 untuk keperluan buat folder
if [ -z "$1" ]; then
  echo "Tulis nama foldernya dulu atuh hehe.."
  exit 1
fi

# Keputusan tergantung apakah folder ada atau tidak
if [ -d "$1" ]; then
  echo "Directory '$1' udah ada cuy hehe.."
else
  mkdir "$1"
  echo "Directory '$1' belum ada, tuh aku buatin hehe.."
fi

# Cek ketersediaan argumen 2
if [ -z "$2" ]; then
  echo "Tapi, sertakan link nya atuh hehe.."
  exit 1w
fi

# Download file dari URL spesifik
cd "$1"
wget "$2"

