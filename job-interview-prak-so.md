# Linux System
Bash (Bourne-Again SHell) adalah bahasa shell dan perintah Unix yang populer yang banyak digunakan di Linux dan sistem operasi berbasis Unix lainnya. Menyediakan Command Line Interface (CLI) yang memungkinkan pengguna untuk berinteraksi dengan sistem dengan mengetikkan perintah ke terminal atau konsol.

Sedangkan Zsh (Z Shell) adalah Unix shell dan bahasa perintah yang mirip dengan Bash tetapi menyediakan fitur tambahan dan opsi penyesuaian. Ini adalah Software Open-source dan dapat digunakan di sebagian besar sistem operasi berbasis Unix, termasuk Linux, macOS, dan FreeBSD.

# 1. Resource Monitoring
Monitoring sumber daya pada sistem operasi (OS) sangat penting, beberapa di antaranya adalah untuk 
- Memastikan kinerja optimal
- Efisiensi penggunaan sumber daya 
- Mengidentifikasi masalah atau hambatan
- Mengantisipasi kegagalan dan melakukan perencanaan kapasitas
## RAM
Secara default menggunakan perintah `free`
```bash
praktikumc@praktikumsistemoperasi ~/1217050121-ridwanskie
 % free
               total        used        free      shared  buff/cache   available
Mem:         2030420      251996      106464         580     1671960     1597800
Swap:              0           0           0
```
Gunakan `free --mega` untuk ditampilkan dalam satuan megabyte
```bash
praktikumc@praktikumsistemoperasi ~/1217050121-ridwanskie
 % free --mega
               total        used        free      shared  buff/cache   available
Mem:            2079         258         109           0        1712        1636
Swap:              0           0           0
```
## CPU
Kita bisa menggunakan `top` untuk melihat penggunaan CPU real-time
![top](https://gitlab.com/if-praktikum-s4/if-prak-so/job-interview/-/raw/main/img/top.png)
Atau `ps -aux` 
```bash
praktikumc@praktikumsistemoperasi ~/1217050121-ridwanskie
 % ps -aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.5 163920 10312 ?        Ss   Apr28   0:35 /lib/systemd/systemd --system --deserialize 36
root           2  0.0  0.0      0     0 ?        S    Apr28   0:00 [kthreadd]
root           3  0.0  0.0      0     0 ?        I<   Apr28   0:00 [rcu_gp]
root           4  0.0  0.0      0     0 ?        I<   Apr28   0:00 [rcu_par_gp]
root           6  0.0  0.0      0     0 ?        I<   Apr28   0:00 [kworker/0:0H-events_highpri]
root           8  0.0  0.0      0     0 ?        I<   Apr28   0:00 [mm_percpu_wq]
root           9  0.0  0.0      0     0 ?        S    Apr28   0:00 [rcu_tasks_rude_]
root          10  0.0  0.0      0     0 ?        S    Apr28   0:00 [rcu_tasks_trace]
root          11  0.0  0.0      0     0 ?        S    Apr28   0:16 [ksoftirqd/0]
root          12  0.0  0.0      0     0 ?        I    Apr28   1:15 [rcu_sched]
root          13  0.0  0.0      0     0 ?        S    Apr28   0:07 [migration/0]
```
## Disk
Gunakan perintah `lsblk` atau `df`, bisa gunakan `df -h` untuk ditampilkan secara _"human readable"_
```bash
praktikumc@praktikumsistemoperasi ~/1217050121-ridwanskie
 % lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
vda     254:0    0   40G  0 disk
├─vda1  254:1    0 39.9G  0 part /
├─vda14 254:14   0    3M  0 part
└─vda15 254:15   0  124M  0 part /boot/efi

 % df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            974M     0  974M   0% /dev
tmpfs           199M  624K  198M   1% /run
/dev/vda1        40G  3.6G   35G  10% /
tmpfs           992M     0  992M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/vda15      124M   11M  114M   9% /boot/efi
tmpfs           199M     0  199M   0% /run/user/1001
```
# 2. Program Management
Dari data monitoring resource kita bisa melakukan tindakan lanjut yaitu manajemen program yang berguna untuk
- Alokasi sumber daya: Untuk mengatur penggunaan sumber daya komputer secara efisien antara program yang berjalan.
- Penjadwalan tugas: Untuk mengatur urutan eksekusi program dan mengoptimalkan penggunaan CPU.
- Keamanan: Untuk melindungi program dan sistem dari interaksi yang tidak diizinkan serta mencegah kerusakan atau gangguan.
- Multiprogramming: Untuk mendukung eksekusi beberapa program secara bersamaan dan mengelola konteks beralih antara program-program tersebut.
- Monitoring dan pemecahan masalah: Untuk memantau aktivitas program, mengumpulkan data, dan memecahkan masalah yang mungkin timbul dalam eksekusi program.
## Memonitor program yang berjalan
Seperti yang telah kita coba sebelumnya, lakukan monitoring program dengan mengeceknya pada CPU. Dengan perintah `top` atau `ps`
## Menghentikan program yang berjalan
Setelah kita monitoring CPU dan menemukan program yang tidak diinginkan, catat PID-nya dan lakukan perintah `kill`
```bash
kill <PID>
```
## Otomasi perintah dengan shell script
Untuk membuat otomasi dengan shellscript, pertama cek bash yang digunakan dengan `which bash`, lalu buat file `.sh`
```bash
 % which bash
/usr/bin/bash

 % touch project-name.sh
```
Buka file `.sh` dengan editor atau menggunakan intruksi `vim`, pada baris pertama gunakan `#! /bin/bash` untuk menentukan interpreter untuk mengeksekusi skrip, dalam hal ini **Bash** shell. 

Masuk mode insert dengan tombol **i** untuk melakukan peng-kodean, untuk kembali ke mode visual tekan tombol **esc** dan ketik `:wq` untuk keluar dan menyimpan perubahan
```bash
 % vim project-name.sh

 #! /bin/bash
```
Mengubah permission dari file project-name.sh, agar program bisa dijalankan.
```bash
chmod +x project-name.sh
```
Jalankan, gunakan perintah
```bash
<directory> /project-name.sh 
```
## Penjadwalan eksekusi program dengan cron
Penjadwalan memiliki banyak manfaat dalam manajemen program, sebagai berikut
- Mencapai efisiensi tenaga dan waktu untuk proses yang komples atau berulang cukup intens
- Merespons permintaan pengguna secara real-time, menjaga kecepatan respons dan pengalaman pengguna yang baik
- Pembersihan file sampah dan backup and restore secara rutin
- Pengambilan data real-time ataupun data statistik per-interval dari sebuah database yang diperlukan oleh aplikasi utama


Untuk melakukannya kita perlu menjalankan perintah `crontab -e`, sistem membuka file crontab di editor teks default yang ditentukan dalam environment variabel  EDITOR. Jika variabel tidak disetel, sistem akan menggunakan editor vi secara default.
```bash
crontab -e
```
cara penggunaan `crontab` adalah sebagai berikut:
```bash
* * * * * command
```

Contoh : Jalankan setiap hari pada 3:30 am
```bash
30 3 * * * /home/praktikumc/1217050121-ridwanskie/job-interview/schedulling.sh

```
# 3. Network Management
Network pada sistem operasi memberikan banyak benefit seperti
- Distributed computing, OS dapat mengatur sumber daya komputer yang tersebar di jaringan dan dikerjakan bersama tim sehingga memungkinkan komputasi paralel, pemrosesan terdistribusi, dan penggunaan klaster komputer 
## Akses sistem operasi pada jaringan menggunakan SSH
Kita bisa membuat koneksi SSH ke komputer jarak jauh dengan membuka terminal di komputer lokal dan memasukkan perintah ssh diikuti dengan alamat IP atau nama domain.
1. Akses dengan ip (only)
    ```bash
    ssh <username>@<public-ip>
2. Akse ip dan port spesifik
    ```bash
    ssh <username>@<public-ip> -p 22
    ```
    
Hasil login via CLI akan terlihat seperti
```bash
C:\Users\Ridwan> ssh praktikumc@103.82.93.37 -p 22
praktikumc@103.82.93.37's password:
 --------------------------------------------
 ----- IDCloudHost Debian 11 (bullseye) -----
 --------------------------------------------
 * Panduan:  https://idcloudhost.com/panduan
 --------------------------------------------
Linux praktikumsistemoperasi 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu May 11 11:47:28 2023 from 125.164.22.46
%
```

## Monitor program yang menggunakan network
Netstat adalah perintah pada Linux dan Unix yang digunakan untuk memantau koneksi jaringan yang aktif dan statistik koneksi jaringan pada suatu sistem, termasuk alamat IP dan nomor port yang sedang digunakan, status koneksi, serta proses yang terkait.
```bash
 % netstat -atnp
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      -
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -
tcp        0      0 127.0.0.1:34487         0.0.0.0:*               LISTEN      474559/node
tcp        0      0 10.29.106.129:22        125.164.21.170:21156    ESTABLISHED -
tcp        0      0 127.0.0.1:34487         127.0.0.1:47684         CLOSE_WAIT  474559/node
tcp        0      0 10.29.106.129:22        114.122.82.144:60323    ESTABLISHED -
tcp        0      0 127.0.0.1:34487         127.0.0.1:55078         ESTABLISHED 474559/node
tcp        0    376 10.29.106.129:22        125.164.19.3:11015      ESTABLISHED -
tcp        0      0 127.0.0.1:55078         127.0.0.1:34487         ESTABLISHED -
tcp        0      0 10.29.106.129:22        125.164.17.118:14125    ESTABLISHED -
tcp        0      0 127.0.0.1:56144         127.0.0.1:34487         ESTABLISHED -
tcp        0      0 10.29.106.129:22        114.122.82.144:60324    ESTABLISHED -
tcp        0      0 10.29.106.129:22        125.164.23.55:23627     ESTABLISHED -
tcp        0      0 127.0.0.1:34487         127.0.0.1:56144         ESTABLISHED 476050/node
tcp        0      0 10.29.106.129:22        125.164.21.227:31962    ESTABLISHED -
tcp        0      0 10.29.106.129:22        125.164.17.40:25501     ESTABLISHED -
tcp6       0      0 :::80                   :::*                    LISTEN      -
tcp6       0      0 :::22                   :::*                    LISTEN      -
```

- "-t": menampilkan koneksi TCP saja.
- "-n": menampilkan alamat IP dan nomor port dalam format numerik.
- "-a": menampilkan semua koneksi, termasuk yang listening dan waiting
- "-p": menampilkan proses yang terkait dengan setiap koneksi.
## Operasi HTTP client
Bisa menggunakan `curl`, misalkan kita akan melakukan curl GET
```bash
curl <specific url>
```
Menggunakan perintah `wget` untuk mengunduh file dari server menggunakan protokol HTTP, HTTPS, dan FTP. **Contoh** mendownload anime Vinland Saga S2 Eps. 11 720p dari Racaty.
```bash
wget https://srv4.racaty.io:183/d/qefglvsz6xjz7x5ir6ljp62dzcquyt3uyy6nvlfw5qoti44xjt65cr4bogacjtubiy75caqm/Otakudesu.lol_Vld.Saga.S2--11_Mkv720p.mkv
```
# 4. File and Folder Management
Penting bagi kita untuk memahami betapa krusialnya peran file dan folder dalam organisasi dan penyimpanan 
- Mengoptimalkan ruang penyimpanan
- Manajemen hak akses
- Mengelola backup and restore file
## Navigasi
Beberapa command yang bisa membantu proses navigasi pada directory
```bash
praktikumc@praktikumsistemoperasi ~/1217050121-ridwanskie
pwd;                # print direktori saat ini
ls;                 # print daftar file dan folder di direktori saat ini
cd; | cd <path>;    # berpindah direktori
```

Hasil, akan terlihat berurutan
```bash
/home/praktikumc/1217050121-ridwanskie
crontab-log  job-interview  kampus  shellscript  stardew-valley  testing

praktikumc@praktikumsistemoperasi ~/1217050121-ridwanskie/job-interview
```
## CRUD file dan folder
- Penggunaan editor

Vim adalah editor teks yang sangat fleksibel dan populer di lingkungan Unix dan Linux. Vim (Vi Improved) adalah pengembangan dari editor vi yang lebih awal, dan menawarkan banyak fitur tambahan
```bash
vim file-name.format-file
```
1. Kita akan berada di mode visual. 
2. Tekan `i` untuk masuk ke mode insert, sehingga bisa melakukan peng-kodean.
3. Jika selesai, tekan tombol `escape` untuk kembali ke mode visual
4. Ketik `:wq` untuk menyimpan perubahan dan keluar dari vim editor
- CRUD file dan folder
Perintah yang bisa digunakan yaitu :
```bash
mkdir
touch 
rm 
rm -rf
rmdir
mv 
cp
ls
cat
```
## Manajemen ownership dan akses file dan folder
Untuk mengelola ownership, akses file dan folder di Linux, Kita dapat menggunakan perintah `chown`, `chmod`, dan `chgrp`.
- **chown**
```bash
 % sudo chown ridwan auto-record.txt
[sudo] password for praktikumc: 
```
- **chmod**

Cara penggunaan dengan format `chmod +<access> <file-name.format>` atau `chmod <***> <file-name.format>` dengan `<***>` diubah menjadi 3 digit untuk akses
```bash
% touch file-name.sh;
% ls -l; 

-rw-r--r-- 1 praktikumc praktikumc   0 May 11 14:38 file-name.sh
 % chmod +x file-name.sh
 % ls -l
 
-rwxr-xr-x 1 praktikumc praktikumc   0 May 11 14:38 file-name.sh
```
## Pencarian file dan folder

Terdapat beberapa CLI yang dapat digunakan untuk melakukan pencarian file dan folder, seperti `find`, `locate`, dan `grep`. 

Untuk `find` digunakan untuk mencari file dan folder berdasarkan kriteria tertentu, seperti nama, ukuran, dan tanggal modifikasi. `locate` adalah utility pencarian cepat yang mencari file dalam database yang diperbarui secara berkala. Sedangkan, `grep` digunakan untuk mencari teks dalam file atau output dari command lain. 
- Pencarian dengan `find`

File
```bash
find /home/praktikumc/1217050121-ridwanskie -name "*.txt"
```
File kosong
```bash
find /home/praktikumc/1217050121-ridwanskie -type f -empty
```
Direktori kosong
```bash
find /home/praktikumc/1217050121-ridwanskie -type d -empty
```
- Pencarian dengan `grep`
```bash
grep "<pattern>" /home/praktikumc/1217050121-ridwanskie/job-interview/example.txt
```
## Kompresi data
Perintah yang digunakan yaitu `tar` dengan contoh ketika ingin mengkompresi seluruh file `.txt` pada direktori saat ini
```bash
tar -czvf file-name.tar.gz *.txt

```

- tar : tape archieve/ kumpulan file tanpa kompresi
- gz : gzip/ file telah dikompresi
- c : create archieve
- z : gzip compress
- v : enable verbosu output/ hasil detail dengan informasi tambahan
- f : name archieve
## Extract file
```bash
tar -xzf file-name.tar.gz

```
- x : extract file compressed
# 5. Shell scripting
Otomasi 
1. [Mendownload file](https://gitlab.com/if-praktikum-s4/if-prak-so/job-interview/-/blob/main/sh/folder-link.sh), lalu menyimpan ke _directory_ yang kita inginkan 
2. [Monitoring user](https://gitlab.com/if-praktikum-s4/if-prak-so/job-interview/-/blob/main/sh/active-user.sh) yang sedang aktif
# 6. Shell script Demonstration
Demo : [via YouTube](https://youtu.be/aFEuSEeWbR8)
# 7. Maze Game
Welcome to the **Genshin Saga** , where you will be challenged to navigate through intricate mazes and solve challenging riddles to achieve your goal.
[**Lets Play**](https://maze.informatika.digital/maze/xavier/)
# 8. Initiative in Project
## Penggunaan tools
Sebagai pengguna windows dan ingin mengakses OS pada sebuah network kita bisa memanfaatkan tools, yaitu menggunakan **Visual Studio Code** dengan extension **Remote SSH** dari **Microsoft**

**Extension** yang digunakan akan terlihat seperti
![Extension Store](https://gitlab.com/if-praktikum-s4/if-prak-so/job-interview/-/raw/main/img/remote-ssh-for-vscode.png)

**Tampilan** saat menggunakan akan terlihat seperti
![Penggunaan](https://gitlab.com/if-praktikum-s4/if-prak-so/job-interview/-/raw/main/img/vscode-ssh.png)



**Why?**
1. VSCode fan-boy
2. Stable SSH connection
3. Look: simple, clean, modern
